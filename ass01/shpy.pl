#!/usr/bin/perl -w

########### - GLOBAL VARIABLES - #############

$index    = 0;
$subFound = 0;
$sysFound = 0;
$osFound  = 0;

################ - SETUP - ###################

foreach $line (<STDIN>) {
    chomp($line);
    push @input, $line;
}

#open( $sh, "<", "$ARGV[0]" ) or die "Can't open file\n";
#chomp( my @input = $sh );
$ast = addNode( $ast, "begin", "begin" );

lexer($ast);
compile($ast);

# Compile turns the AST into
sub compile {
    my ($ast) = @_;

    if ( $ast->{KEYWORD} eq "fi" ) { pop @stack; }

    if ( ( !defined $ast->{NESTED} ) && ( !defined $ast->{INLINE} ) ) {
        pop @stack;
        return;
    }

    if ( defined $ast->{NESTED} ) {
        push @stack, $ast;
        foreach (@stack) {
            print "    ";
        }
        translate( $ast->{NESTED} );

        #print "$ast->{NESTED}->{KEYWORD} : $ast->{NESTED}->{ARG}\n";
        compile( $ast->{NESTED} );
    }

    if ( ( defined $ast->{INLINE} ) ) {
        foreach (@stack) {
            print "    ";
        }
        translate( $ast->{INLINE} );

        #print "$ast->{INLINE}->{KEYWORD} : $ast->{INLINE}->{ARG}\n";
        compile( $ast->{INLINE} );
    }

    return;

}

# The lexar splits the file into an AST
#
# Receives a hash table reference
#
# Hash table stores the syntax word as the key and the arg as the contents
# When we reach a keyword (if, for, while, etc) we recurse and parse the current hash table
# We return if we find a terminator (done, fi etc)
#
# Must split each line at a ' ' or a '='.
# Ignore Lines beginning with #
#
#
#
sub lexer {

    #print "LEXER BEGINNING...\n\n";
    my ($ast) = @_;

    my $keyword;
    my $cond;
    my $mode;
    while ( $index < @input ) {
        $line = $input[$index];
        $line =~ s/^\s+//g;
        $line =~ s/\s+$//g;
        $index++;
        if (   ( defined $ast )
            && ( $ast->{KEYWORD} =~ /for|if|else|elif/ )
            && ( !defined $ast->{NESTED} ) )
        {
            $mode = "NESTED";
        }
        else {
            $mode = "INLINE";
        }

        if ( $line =~ / \$[12345]/ ) {
            $sysFound = 1;
        }

        if ( $line =~ /^fi|^done/ ) {
            $ast = $stack[-1];
            $ast->{INLINE} = addNode( $ast->{INLINE}, "fi", '' );
            $ast = $ast->{INLINE};
            pop @stack;
        }
        if ( $line =~ /^else$/ ) {
            $ast = $stack[-1];
            $ast->{INLINE} = addNode( $ast->{INLINE}, "else", '' );
            $ast = $ast->{INLINE};
            pop @stack;
        }
        elsif ( $line =~ /^do.*$|^then.*$/ ) {
            next;
        }
        elsif ( $line =~ /^elif / ) {
            ( $keyword, $cond ) = split( /\s/, $line, 2 );
            $ast = $stack[-1];
            pop @stack;
            $ast->{$mode} = addNode( $ast->{$mode}, $keyword, $cond );
            $ast = $ast->{$mode};
            push @stack, $ast;
        }

        elsif ( $line =~ /^for |^if / ) {
            ( $keyword, $cond ) = split( /\s/, $line, 2 );
            $ast->{$mode} = addNode( $ast->{$mode}, $keyword, $cond );
            $ast = $ast->{$mode};
            push @stack, $ast;
        }
        else {
            if ( $line =~ /echo/ ) {
                ( $keyword, $cond ) = split( /\s/, $line, 2 );
            }
            elsif ( $line =~ /^.+=.+$/ ) {
                $keyword = '=';
                $cond    = $line;
            }
            elsif ( $line =~ /^#/ ) {
                $keyword = '#';
                $cond    = $line;
            }
            elsif ( $line =~ /^exit/ ) {
                $sysFound = 1;
                $keyword  = 'exit';
                $line =~ s/^exit //;
                $cond = $line;
            }
            elsif ( $line =~ /^read/ ) {
                $sysFound = 1;
                $keyword  = 'read';
                $line =~ s/^read //;
                $cond = $line;
            }
            elsif ( $line =~ /^cd/ ) {
                $osFound = 1;
                $keyword = 'cd';
                $line =~ s/^cd //;
                $cond = $line;
            }
            elsif ( $line =~ /^pwd|^ls/ ) {
                $keyword  = 'subprocess';
                $cond     = $line;
                $subFound = 1;
            }
            if ( $line eq '' ) {    # This handles blank lines!
                $keyword = 'newline';
                $cond    = '';
            }
            $ast->{$mode} = addNode( $ast->{$mode}, $keyword, $cond );
            $ast = $ast->{$mode};
        }

    }

}

sub translate {
    my ($node) = @_;
    my $arg = '';

    if ( $node->{KEYWORD} eq "#" ) {

        if ( $node->{ARG} eq "#!/bin/bash" ) {
            print "#!/usr/bin/python2.7 -u\n";
            if ($subFound) {
                print "import subprocess\n";
            }
            if ($sysFound) {
                print "import sys\n";
            }
            if ($osFound) {
                print "import os\n";
            }
        }
        else {
            print "$node->{ARG}\n";
        }
    }

    # ECHO #
    if ( $node->{KEYWORD} eq "echo" ) {
        print "print ";
        if ( $node->{ARG} =~ /^'.*'$|^".*"$/ ) {
            print "$node->{ARG}\n";
        }
        else {
            @args = split( /\s/, $node->{ARG} );
            foreach (@args) {
                if ( $_ =~ /^\$.+/ ) {
                    $_ = reverse($_);
                    chop $_;    #REMOVE '$'
                    $_ = reverse($_);
                    if ( $_ =~ /[12345]/ ) {
                        $_ = "sys.argv[$_]";
                    }
                    $arg = "$arg$_, ";
                }
                else {
                    $arg = "$arg'$_', ";
                }
            }
            $arg =~ s/, +$//;
            print "$arg";
            print "\n";
        }
    }

    if ( $node->{KEYWORD} eq "for" ) {
        print "for ";
        @args = split( / in /, $node->{ARG} );
        print "$args[0] in ";
        @args = split( /\s/, $args[1] );
        foreach (@args) {
            if ( $_ =~ /^\$.+/ ) {
                $_ = reverse($_);
                chop $_;
                $_ = reverse($_);
                if ( $_ =~ /[12345]/ ) {
                    $_   = "sys.argv[$_]";
                    $arg = "$arg$_, ";
                }
                else {
                    $arg = "$arg$_ ";
                }

                $arg = "$arg$_, ";
            }
            elsif ( $_ =~ /^[1234567890]+$/ ) {
                $arg = "$arg$_, ";
            }

            else {
                $arg = "$arg\"$_\", ";
            }
        }
        chop $arg;
        chop $arg;
        print "$arg:\n";
        $arg = '';
    }

    if ( $node->{KEYWORD} =~ /(el)*if$/ ) {
        print "$node->{KEYWORD} ";
        @args = split( / /, $node->{ARG} );
        foreach (@args) {
            if ( $_ eq '=' ) {
                $arg = "$arg == ";
            }
            elsif ( $_ eq 'test' ) {
                next;
            }
            else {
                if ( $_ =~ /^\$.+/ ) {
                    $_ = reverse($_);
                    chop $_;
                    $_ = reverse($_);
                    if ( $_ =~ /[12345]/ ) {
                        $_ = "sys.argv[$_]";
                    }
                    $arg = "$arg$_ ";

                }
                elsif ( $_ =~ /^[1234567890]+$/ ) {
                    $arg = "$arg$_ ";
                }

                else {
                    $arg = "$arg'$_' ";
                }
            }

        }
        chop $arg;
        print "$arg:\n";

    }

    if ( $node->{KEYWORD} eq "else" ) {
        print "else:\n";
    }

    if ( $node->{KEYWORD} eq "subprocess" ) {
        @args = split( /\s/, $node->{ARG} );
        print "subprocess.call([";
        foreach (@args) {
            $arg = "$arg\'$_\', ";
        }
        chop $arg;
        chop $arg;
        print $arg;
        print "])\n";
    }

    if ( $node->{KEYWORD} eq "=" ) {
        @args = split( /=/, $node->{ARG} );
        if ( $args[1] =~ /^[1234567890]+$/ ) {
            print "$args[0] = $args[1]\n";
        }
        else {
            print "$args[0] = \'$args[1]\'\n";
        }
    }
    if ( $node->{KEYWORD} eq "newline" ) {
        print "\n";
    }
    if ( $node->{KEYWORD} eq "read" ) {
        print "$node->{ARG} = sys.stdin.readline().strip()\n";
    }
    if ( $node->{KEYWORD} eq "exit" ) {
        print "sys.exit($node->{ARG})\n";
    }
    if ( $node->{KEYWORD} eq "cd" ) {
        print "os.chdir('$node->{ARG}')\n";
    }

}

#### CODE FOR BST ####

sub addNode {

    my ( $ast, $keyword, $arg ) = @_;

    $ast            = {};
    $ast->{KEYWORD} = $keyword;
    $ast->{ARG}     = $arg;
    $ast->{INLINE}  = undef;
    $ast->{NESTED}  = undef;

   #        print ("AST ADDED\n KEYWORD: $ast->{KEYWORD}\n ARG: $ast->{ARG}\n");
    return $ast;
}

