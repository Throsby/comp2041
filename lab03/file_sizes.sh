#!/bin/bash

# Lab03 Excersise 3: File Size
# 
# Written By Jack Bennett 09/08/15

text=$(ls | sort)
len=$(wc -l $text | wc -l) 
big_string="Large files:"
small_string="Small files:"
medium_string="Medium-sized files:"
space=" "
i=1


while (( $i < $len )) 
do
	file=$(echo $text | cut -d' ' -f$i)
 	file_len=$( cat $file | wc -l )
	if (( $file_len < 10 ));	then
		small_string=$small_string$space$file
	
	elif (( $file_len < 100 )) && (( $file_len >= 10 )); then
		medium_string=$medium_string$space$file
	
	else
		big_string=$big_string$space$file
	fi
	
	i=$(($i + 1))
	#sleep 1 
done

echo $small_string
echo $medium_string
echo $big_string
