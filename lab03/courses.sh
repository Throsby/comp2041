#!/bin/sh

course=$1
search_char=`echo $course | cut -c 1`
undergrad=`wget -q -O- "http://www.handbook.unsw.edu.au/vbook2015/brCoursesByAtoZ.jsp?StudyLevel=Undergraduate&descr=$search_char"`
postgrad=`wget -q -O- "http://www.handbook.unsw.edu.au/vbook2015/brCoursesByAtoZ.jsp?StudyLevel=Postgraduate&descr=$search_char"`
printf "%s\n%s" "$undergrad" "$postgrad" | egrep "$course.....html" | cut -d'/' -f7 | sed 's/.html">/ /g' | rev | cut -d'<' -f2 | rev | sort |  sed 's/ *$//g' | uniq

