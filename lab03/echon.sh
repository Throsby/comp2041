#!/bin/bash

# Lab03 Excersise 2: Repeated Echo
#
# Written By Jack Bennett 07/08/15


i=$1
if [ -z "$1" ] || [ -z "$2" ] || [ ! -z "$3" ] ; then
    echo "Usage: ./echon.sh <number of lines> <string>"
else
    if (( $i < 0 )) || [[ $i =~ ^[^(0-9)]+$ ]] ; then
        echo "./echon.sh: argument 1 must be a non-negative integer"
    else

        while (( $i > 0 )) 
        do
            i=$((i-1))      ### Increments the index counter
            echo $2         ### Prints the second command line arg
            #sleep 1        ### For Debugging
        done
    fi
fi
