#!/bin/bash

# Lab03 Excersise 2: Digits
#
# Written By Jack Bennett 08/08/15


while read LINE; do
	index=0
	len=${#LINE}	
	string=${LINE}
	echo $LINE | sed -e 's:[0-4]:<:g' | sed -e 's:[6-9]:>:g'

done

exit 0 
