#!/bin/bash

text=`ls | egrep ".jpg$"`
i=`ls | egrep ".jpg$" | wc -l `
j=1
while(($j <= $i)) 
do
	temp=`printf "%s" "$text" | head -n $j | tail -n 1 | cut -d"." -f1`
	if (( `ls | egrep "$temp".png  | wc -l` == 0 )); then
		convert "$temp".jpg "$temp".png
		rm "$temp".jpg
	else 
		echo "$temp.png already exists"
		exit 1
	fi
	j=$((j+1))
done

