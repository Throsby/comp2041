#!/bin/bash

for image in "$@"
do
	display $image
	printf "Address to e-mail this image to? "
	read -r email
	if ((`echo $email | egrep '^.+@.+$' | wc -l` == 0)); then
		echo "Invalid e-mail"
		exit 1
	fi 
	printf "Message to accompany image? "
	read -r message
	
	echo $message | mutt -s $image -a $image -- $email
done
