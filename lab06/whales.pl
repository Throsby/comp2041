#!/usr/bin/perl -w
#

@searchResults = ();
$searchStr     = $ARGV[0];
$total         = 0;
$pods          = 0;

    foreach $line ( <STDIN> ) {
       if ( $line =~ $searchStr ) {
           push @searchResults, $line;
       }
   }

   foreach $entry ( @searchResults ) {
       @subStrings = split / /, $entry;
       $numWhales  = $subStrings[0];
       $total      = $total + $numWhales;
       $pods++;

   }

   print "$searchStr observations: $pods pods, $total individuals\n";

   
