#!/usr/bin/perl -w

my @arr;

# Convert to lower case and remove trailing spaces
# Add All Whales to array
# If already in array add the number to the total and increment pod array
# Each index of whale array should corrospond to the total and pod array indexes.
# Cat these all together to form the output strings, sort them and then print.

@whales = ();
@pods = ();
@total = ();
$index = 0;
$arrSize = 0;

foreach $line ( <> ) {
    $line =~ s/\s+$//;
    $line =~ s/\s+/ /g;
    @search = split(/\s/, $line, 2);
    
    chomp $search[1];
    $search[1] = lc($search[1]);         #Lowers case.
    if ($search[1] =~ ".*s\$") { chop($search[1]); } 


    while ($index <= $arrSize ) {
        if( $arrSize == 0 ){
            push @whales, $search[1];
            push @pods, 1;
            push @total, $search[0];
            $arrSize++;
            last;
        } 
        if( $index == $arrSize ) {
            push @whales, $search[1];
            push @pods, 1;
            push @total, $search[0];
            $arrSize++;
            last;
        }
        if($search[1] eq $whales[$index]) {
            $pods[$index]++;
            $total[$index] += $search[0];
            last;
        } 
        $index++;
    } 
    $index = 0;
}

# HERE WE PUSH ALL THE OUTPUTS ONTO AN ARRAY AND THEN SORT THEM #

foreach ( @whales ) {
    push @unsortedOut, "$whales[$index] observations:  $pods[$index] pods, $total[$index] individuals\n";
    $index++;
}

my @sortedOut = sort { lc($a) cmp lc($b) } @unsortedOut;

foreach $out (@sortedOut) {
   print $out
}


