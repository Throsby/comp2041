#!/usr/bin/perl -w

$search = $ARGV[0];
$search = lc($search);
$total = 0;
my %freq;
my %totals;

foreach $file ( glob "poets/*txt"  ) {
    $total = 0;
    $poet = $file;
    $poet =~ s/poets\///;
    $poet =~ s/.txt$//;
    $poet =~ s/_/ /g; 

    foreach $arg (@ARGV) {
        $freq{$poet}{$arg} = 1;
    }

    open(my $f, "<", $file) or die;
    while(<$f>) {
        @words = split(/[^a-zA-Z]/, $_);
        foreach $word ( @words ){
            $word = lc($word);
            if($word =~ m/[a-zA-Z]+/){
                        $freq{$poet}{$word}++;
                        $total++;
            }
        }
    } 
    $totals{$poet} = $total;   

}

foreach $poet (sort keys %freq) {
    $prob = 0;
    foreach $arg (@ARGV){
        $frequency = $freq{$poet}{$search}/$totals{$poet};
        $prob += log($frequency);
    }

    printf ("log((%d+1)/%6d) = %8.4f %s\n", ($freq{$poet}{$search}-1), $totals{$poet}, $prob, $poet);     
 }
