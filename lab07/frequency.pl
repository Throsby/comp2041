#!/usr/bin/perl -w

$search = $ARGV[0];
$search = lc($search);
$total = 0;
my %freq;
my %totals;

foreach $file ( glob "poets/*txt"  ) {
    $total = 0;
    $poet = $file;
    $poet =~ s/poets\///;
    $poet =~ s/.txt$//;
    $poet =~ s/_/ /g; 
    $freq{$poet}{$search} = 0; 
    open(my $f, "<", $file) or die;
    while(<$f>) {
        @words = split(/[^a-zA-Z]/, $_);
        foreach $word ( @words ){
            $word = lc($word);
            if($word =~ m/[a-zA-Z]+/){
                        $freq{$poet}{$word}++;
                        $total++;
            }   
                
        }
    } 
    $totals{$poet} = $total;   

}

foreach $poet (sort keys %freq) {
     printf ("%4d/%6d = %.9f %s\n", $freq{$poet}{$search}, $totals{$poet}, $freq{$poet}{$search}/$totals{$poet}, $poet);
 }
