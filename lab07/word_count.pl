#!/usr/bin/perl -w

$search = $ARGV[0];
$found = 0;

foreach $line ( <STDIN> ) {
    @words = split(/[^a-zA-Z]/, $line);
    foreach $word ( @words ){
        $word = lc($word);
        if ($word eq $search) {
            $found++;
        }
    }
}

print "$search occurred $found times\n";
