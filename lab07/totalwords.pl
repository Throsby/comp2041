#!/usr/bin/perl -w
#

$total = 0;

foreach $line ( <STDIN> ) {
    @words = split(/[^a-zA-Z]/, $line);
    foreach $word ( @words =~ /[a-zA-Z]/ ){
        $total++;
        }
    }
}

print "$total words\n";
