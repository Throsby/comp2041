#!/usr/bin/perl -w

my %freq;
my %totals;
my %probs;

foreach $file ( glob "poets/*txt"  ) {
    $poet = $file;
    $poet =~ s/poets\///;
    $poet =~ s/.txt$//;
    $poet =~ s/_/ /g; 

    open(my $f, "<", $file) or die;
    while(<$f>) {
        @words = split(/[^a-zA-Z]/, $_);
        foreach $word ( @words ){
            $word = lc($word);
            if($word =~ m/[a-zA-Z]+/){
                        if(!exists $freq{$poet}{$word}) { $freq{$poet}{$word}++;}

                        $freq{$poet}{$word}++;
                        $totals{$poet}++;
            }
        }
    } 

}


foreach $arg (@ARGV) {
    foreach $poet (sort keys %freq) {
        $probs{$poet} = 0;
    }
    foreach $file ( glob "$arg" ) {
        open(my $f, "<", $file) or die;
        while(<$f>) {
            @words = split(/[^a-zA-Z]/, $_);
            
            foreach $poet (sort keys %totals) {
                foreach $word ( @words ){
                    $word = lc($word);
                    if($word =~ m/[a-z]+/){
                            if(!exists($freq{$poet}{$word})){ $freq{$poet}{$word} = 1; }
                            if ($probs{$poet} == 0) {

                                $frequency = $freq{$poet}{$word}/$totals{$poet};
                                $probs{$poet} = log($frequency);
                            }
                            else { 
                            $probs{$poet} = $probs{$poet} + log($freq{$poet}{$word}/$totals{$poet});  
                        } 
                    }    
                }
            }
        }
    }

    $most_likely = '0';
    foreach $poet (sort keys %probs) {
        if( $most_likely eq '0' ) { 
            $most_likely = $poet;
        }
        else {
            if ($probs{$poet} > $probs{$most_likely}) {
                $most_likely = $poet;
            }
        }
    }

    printf("%s most resembles the work of %s (log-probability=%4.1f)\n", $arg, $most_likely, $probs{$most_likely});  

}



