#!/usr/bin/perl -w

$echos=$ARGV[0];
$word=$ARGV[1];

if ( @ARGV != 2 ) {
    print "Usage: ./echon.pl <number of lines> <string>\n";
}
else{
    while ($echos > 0) {
        print "$word\n";
        $echos--;
    }
}
    


