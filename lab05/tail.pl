#!/usr/bin/perl -w
$count = 0;
@files = ();
$m = 9;
$numArgs = @ARGV;

if(@ARGV == 0){
    while (<>) {
        chomp($_);
        push(@stdArray, $_);
    }
    @revArray = reverse(@stdArray);
    $n = $m;

    while($n >= 0) {
        print "$revArray[$n]\n";
        $n--;    
   }
        exit 0;
   
}


if($ARGV[0] =~ m/^-[1234567890]+$/){
        $m = $ARGV[0]; 
        $m =~ s/-//g;
        $count++;
        $m--;
        $numArgs--;
    }

while ($count < @ARGV) {
    # if i = 0 && regex fits the flag
    # then change n.
    push @files, $ARGV[$count];
    $count++;
}

foreach $f (@files) {
   if ($numArgs > 1){
    print "==> $f <==\n";
}   
   $n = $m;
   open(my $FILE, "<$f") or die "./tail.pl: can't open $f";
   chomp(my @lines = <$FILE>);
   @revLines = reverse(@lines);
   
   if (@revLines < $n) {
          $n = @revLines;
          $n--;
      }
   while ($n >= 0 ) {
        print "$revLines[$n]\n";
        $n--;
         }
 }
