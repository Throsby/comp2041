# README #

Created this repo for COMP2041:Software Construction to encourage crowd sourcing and group learning.


### What is this repository for? ###

* Lab answers for COMP2041.
* For personal use only.


### Contribution guidelines ###

* Provide better solutions for lab questions.
* Code review and commenting.
* Constructive feedback.

### Who do I talk to? ###

* Jack Bennett
* Email: z3484290@zmail.unsw.edu.au